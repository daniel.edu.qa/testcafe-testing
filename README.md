# Saucedemo exercise

## Running the tests

1. The tests can be run with the next command, by replacing `<browser>` with the desired browser and `<fileName/directory>` with the file or path of the desired tests:
```
testcafe <browser> <fileName/directory>
```

in our case it is:
```
testcafe chrome test-purchase-flow.js
```

2. The tests can also be run by using `nvm` commands with the scripts I have specified in `package.json`:
```
npm run test:chrome
```
or
```
npm run test:chrome:headless
```

## problem_user flow

I did run the test flow with the `problem_user` and based on the items used in this case, the test failed at counting the number of items in the cart as one of them wasn't added. Being an automated test flow, it would fail at the first issue so I decided to take a closer look with a `manual` approach.

The issues I noticed:

1. All the items have the same (wrong) image on the inventory page.
2. Only 3 items can be added in the cart.
3. Items cannot be removed from the inventory page.
4. The links to the items are all messed up. (ex: clicking on `Sauce Labs Bolt T-Shirt` will redirect to `Sauce Labs Onesie`)
5. The previously specified item is the only one that can be added to cart and removed from cart from the item page.
6. Clicking on `Sauce Labs Fleece Jacket` redirects to a `ITEM NOT FOUND` page. Adding this item to cart and continuing to the cart will result in a blank page.
7. Sorting is not working.
8. You cannot go further the `Checkout: Your Information` => when typing in the `Last Name` input it replaces the value of the `First Name` with the last character that was typed, the `Last Name` input staying empty. So when clicking on `Continue` we receive the `Error: Last Name is required`.

## Comments on the exercise

### Test file structure

1. At the start of the file I have defined the test data for the user and the items to be used.
2. After that we have the functions that we are using in the test.
3. At the end is the actual test also with beforeEach and afterEach.

### Conclusion

I kept everything in the `test-purchase-flow.js` file as specified in the requirements. I hope I didn't missunderstand that.

In a real project I would most probably use the `Page Object` model with the `BaseTestClass` and move the `Selectors` and the `functions` out of the test file.

I could have done the exercise in so many ways, but I thought it would be better to have parameterised functions and not just hardcode everything. I expect I didn't overcomplicate it.

### One issue I could not solve

When running with `chrome`, the browser does not maximize the window on my computer. I added the `await t.maximizeWindow();` step in `beforeEach` but it conflicts with the script `test:chrome:mobile`.

Tried it also with `--start-fullscreen` and it didn't work.

I didn't put too much effort into solving this, but I am sure there are other ways to solve it.
