import { Selector, ClientFunction, t } from 'testcafe';

// define user test data
const standardUserData = {
    username: 'standard_user',
    password: 'secret_sauce',
    expectedDisplayName: 'Standard User',
    firstName: 'Standard',
    lastName: 'User',
    postalCode: '1234'
  };

// define items test data
const items = [
    {name: 'Sauce Labs Backpack', price: '$29.99'},
    {name: 'Sauce Labs Fleece Jacket', price: '$49.99'},
    {name: 'Sauce Labs Bike Light', price: '$9.99'}
];

// get current url
const getURL = ClientFunction(() => window.location.href);

// add an item to cart
const addToCart = async (itemName) => {
    // get the item selector based on the name
    const item = Selector('div.inventory_item_name').withExactText(itemName);
    
    // get the addToCart button for the specific selected item
    const addToCartButton = item.parent().find('button.btn_inventory');
  
    await t.click(addToCartButton);
  }

// verify the items in the cart
async function verifyCartItems(expectedItems) {
    // get the cart item elements
    const cartItems = Selector('.cart_item');

    // verify that count of cartItems is equal to the number of args specified
    await t.expect(cartItems.count).eql(expectedItems.length, `Expected ${expectedItems.length} items in the cart`);
    
    // verify that all items are in the cart
    for (let i = 0; i < expectedItems.length; i++) {
        const itemName = await cartItems.nth(i).find('.inventory_item_name').innerText;
        await t.expect(itemName).eql(expectedItems[i].name, `Item "${expectedItems[i].name}" not found in the cart.`);
    }
}

// verify the total price
async function verifyTotalPrice(expectedItems) {
    // get the cart item elements
    const cartItems = Selector('.cart_item');

    // loop through the cart items and check their prices
    let totalPrice = 0;
    for (let i = 0; i < expectedItems.length; i++) {
        const item = expectedItems[i];
        const cartItem = cartItems.nth(i);
        const itemPrice = await cartItem.find('.inventory_item_price').innerText;
        await t.expect(itemPrice).eql(item.price, `The expected price was ${item.price}`);

        // sum the prices
        totalPrice += parseFloat(item.price.replace('$', ''));
    }

    // calculate subtotal + 8% tax
    totalPrice = (totalPrice * 1.08).toFixed(2);

    // check that the total price is correct
    const cartTotal = (await Selector('.summary_total_label').withText('Total:').innerText).replace('Total: $', '');
    await t.expect(cartTotal).eql(totalPrice, `Total price including tax should be ${totalPrice}`);
}

fixture `SauceDemo Purchase Flow`
    .page `https://www.saucedemo.com/`
    .beforeEach(async t => {
    // i. Navigate to the login page and log in as the standard user
    // await t.maximizeWindow();
    const usernameInput = Selector('#user-name');
    const passwordInput = Selector('#password');
    const loginButton = Selector('#login-button');

    await t.typeText(usernameInput, standardUserData.username);
    await t.typeText(passwordInput, standardUserData.password);
    await t.click(loginButton);
    })
    .afterEach(async t => {
    // last step. Logout from the application
    const burgerMenuButton = Selector('#react-burger-menu-btn');
    const logoutLink = Selector('#logout_sidebar_link');

    await t.click(burgerMenuButton);
    await t.click(logoutLink);
    })

test('Complete Purchase Flow as Standard User', async t => {
    // ii. Verify that the user is redirected to the inventory page
    const pageTitle = Selector('.title'); // this selector will be further used for checking the step in the flow
    const inventoryContainer = Selector('.inventory_container');
    const url = await getURL();
    
    // multiple ways of verifying the inventory page
    await t.expect(pageTitle.innerText).eql('Products');
    await t.expect(inventoryContainer.exists).ok();
    await t.expect(url).contains('/inventory');

    // iii. Add three items to the cart and verify that the cart counter shows the correct number of items
    await addToCart(items[0].name);
    await addToCart(items[1].name);
    await addToCart(items[2].name);

    const cartLink = Selector('.shopping_cart_link');
    await t.expect(cartLink.innerText).eql('3');

    // iv. Navigate to the cart page and verify that the correct items are in the cart
    await t.click(cartLink);

    await t.expect(pageTitle.innerText).eql('Your Cart');
    await verifyCartItems(items);

    // v. Proceed to the checkout page, enter customer information, and click continue
    const checkoutButton = Selector('#checkout');

    await t.click(checkoutButton);
    await t.expect(pageTitle.innerText).eql('Checkout: Your Information');

    const firstNameInput = Selector('#first-name');
    const lastNameInput = Selector('#last-name');
    const postalCodeInput = Selector('#postal-code');
    const continueButton = Selector('#continue');

    await t.typeText(firstNameInput, standardUserData.firstName);
    await t.typeText(lastNameInput, standardUserData.lastName);
    await t.typeText(postalCodeInput, standardUserData.postalCode);
    await t.click(continueButton);

    // vi. Verify that the user is redirected to the checkout overview page and the summary contains the correct items and total
    await t.expect(pageTitle.innerText).eql('Checkout: Overview');
    await verifyCartItems(items);
    await verifyTotalPrice(items);

    // vii. Complete the purchase and verify that the user is redirected to the order confirmation page
    const finishButton = Selector('#finish');

    await t.click(finishButton);
    await t.expect(pageTitle.innerText).eql('Checkout: Complete!');

    // viii. Confirm that the order confirmation message is displayed, indicating a successful purchase
    const confirmationHeader = Selector('.complete-header').innerText;
    const confirmationMessage = Selector('.complete-text').innerText;

    await t.expect(confirmationHeader).eql('Thank you for your order!');
    await t.expect(confirmationMessage).eql('Your order has been dispatched, and will arrive just as fast as the pony can get there!');
})
